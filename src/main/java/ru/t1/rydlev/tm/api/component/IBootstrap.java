package ru.t1.rydlev.tm.api.component;

public interface IBootstrap {

    void run(String... args);

}
